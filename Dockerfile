FROM eddelbuettel/r2u:20.04
WORKDIR /docs
COPY *.qmd /docs
COPY *.y* /docs
COPY gxfsfr_files /docs
RUN apt-get update && apt-get install -y --no-install-recommends \
    pandoc \
    pandoc-citeproc \
    curl \
    gdebi-core \
    && rm -rf /var/lib/apt/lists/*
#RUN apt-get install libnss
#RUN apt-get install libnss3-dev libgdk-pixbuf2.0-dev libgtk-3-dev libxss-dev
RUN apt install -y libnss3
RUN install.r \
    shiny \
    jsonlite \
    ggplot2 \
    htmltools \
    remotes \
    renv \
    knitr \
    rmarkdown \
    quarto
RUN curl -LO https://quarto.org/download/latest/quarto-linux-amd64.deb
RUN gdebi --non-interactive quarto-linux-amd64.deb
RUN quarto tools install chromium
RUN quarto install tinytex
WORKDIR /docs
RUN quarto render 
CMD ["bash"]
