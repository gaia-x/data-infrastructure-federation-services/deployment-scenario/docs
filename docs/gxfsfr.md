# Purpose of the this Document 

The goal of this document is to explain how the Compliance and Labelling is implemented with the current Ontology described in the WG Service Caracteristics. </br>
This document is using Labelling and Compliance defined by the [Policy Rules and Labelling Documents](https://docs.gaia-x.eu/policy-rules-committee/policy-rules-labelling/22.11/policy_rules_labeling_document/) and does not replace it. 

# Context 

The TrustFramework 22.10 outlines the compliance and labelling requirements for participants, services, and locations within the Gaia-X ecosystem. Compliance ensures the legality and adherence to rules and regulations, while labelling provides clear information on the service characteristics and properties. Verifiable credentials (VCs) are issued for compliance and labelling, with compliance being mandatory and labelling strongly recommended.

The compliance process involves validating the consistency and veracity of data in input, which may include Verifiable Presentations for Located Service Offering, legal Participant, and Provider. This process involves checking attribute consistency, proof consistency, and verifying the document's DIDs against trust_anchors.

There are specific compliance processes for legal persons (participants) and located service offerings or service offerings, each with a set of rules that must be validated. Gaia-X labelling involves meeting a minimum baseline of criteria to be part of the ecosystem, ensuring common governance and basic interoperability levels. There are three label levels, each with specific criteria and verification processes, focusing on aspects such as contractual governance, data protection, cybersecurity, portability, and European control.

All the compliance Participant (Legal person or Natural Person) and compliance ServiceOffering informations come from [TrustFramework 22.10](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/)  
The Labelling process comes from [Policy Rules and Labelling Documents](https://docs.gaia-x.eu/policy-rules-committee/policy-rules-labelling/22.11/policy_rules_labeling_document/)

# Compliance and Labelling

Compliance to the TrustFramework 22.10 is the set of rules and regulations with which a participant, service or location MUST comply. It can relate to different areas. Compliance with these rules is essential to ensure the legality of the participant, service or location.  

Labelling is the trusted reference appearing on the services intended for users. Its purpose is to provide the user with clear and precise information on the characteristics and properties of the service. Labelling MUST be defined by international regulations. 
 
If compliance is validated, a dedicated verifiable credential will be issued. VC compliance is mandatory to be part of gaia-x. If labelling criterias are met, a dedicated verifiable credential will be issued. Having a label is strongly recommended but it is not mandatory.

The Compliance Service validates the schemas provided by the gaia-x registry, content and credentials of Self Descriptions. The service issues a Verifiable Credential attesting of the result. Required fields and consistency rules are defined in the [Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/).

## General Compliance Process

The primary objective of the compliance process and its associated procedures is to authenticate the consistency and accuracy of the data provided as input. This input data MAY pertain to a Verifiable Presentation for a Located Service Offering, a legal Participant, or a Provider.

Firstly, it is essential to ensure the consistency of the attributes. This involves checking the input data conforms to the specifications in terms of context and type. For instance, a valid Participant must have some attributes as defined in the [Participant](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/participant/#legal-person) structure.

Secondly, proofs have to be consistent. Both Verifiable Credentials and Verifiable Presentations must include a proof, which serves as a digital signature. These proofs must conform to a specific format, which is, in turn, associated with the Trust Framework. The Trust Framework is a set of policies, standards, and protocols that define the rules for issuing, managing, and verifying digital identities and credentials.

Lastly, the compliance process examines all the Decentralized Identifiers (DIDs) within the document and confirms whether these public keys are listed among the trust anchors. Trust anchors are trusted entities responsible for validating the authenticity of digital signatures and ensuring the secure exchange of informations.

If all the requirements are satisfied, the compliance service can proceed to the issuance of a dedicated Verifiable Credential corresponding to the Compliance Object. This Verifiable Credential serves as a digital attestation of the authenticity, integrity, and validity of the data provided, thereby instilling confidence in the involved parties and streamlining their interactions.

## Participant Compliance for Legal Person
  
A Participant is a Legal Person or Natural Person, which is identified, onboarded and own its Gaia-X Self-Description. Instances of a Participant neither being a legal nor a natural person are prohibited. [cf. Particpant TrustFramework 22.10](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/participant/)

The Legal Person Compliance checks if the participant's verifiable credential is compliant with all technical Gaia-x requirements. 

[Here](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/participant/#legal-person) is the reference of participant object.

The table below show all the rules provided by Gaia-X for the participant VC. Each of these rules MUST be validated.

| Rule name | Description |
| --------- | ----------- |
| CPR-01_checkJsonLd | Check json-ld validity |
| CPR-02_checkSchema | Check gaia-x [schemas](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-participant/legal-person.yaml) gax-core and gax-participant used and verified |
| CPR-03_checkSignature | Check signatures validity as defined by the TrustFramework |
| CPR-04_checkRegNum | If legalAddress.country is located in the European Economic Area, Iceland, Lichtenstein and in Norway, then registrationNumber MUST be at least one of the following types: EORI, leiCode, local, vatID. To check the value of these types, please use the APIs: https://docs.gaia-x.eu/policy-rules-committee/trust-framework/latest/trust_anchors/#for-registrationnumberissuers-trust-anchors. If more than one type is registered, check the consistency of the informations. |
| CPR-05_checkDid | All DID are resolvable using a did resolver. Did example : did.web, did:key, did:ebsi etc  <br>If a did:web contains a [path](https://www.w3.org/TR/did-core/#dfn-did-paths), [fragment](https://www.w3.org/TR/did-core/#dfn-did-fragments) and/or [query](https://www.w3.org/TR/did-core/#dfn-did-queries) the resolver will ignore these informations |
| CPR-06_checkTracability | The keypair used to sign the Data Resource claims MUST be traceable to the producedBy participant of the Data Resource. [cf. TrustFramework](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/resource/#data-resource) |
| CPR-07_checkContainsPII | If containsPIIis true, attributes related to the Legitimate Processing of Information related to PII become mandatory. [cf. TrustFramework](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/resource/#data-resource) |

## Located Service Offering or Service Offering Compliance
  
The Located Service Offering or Service Offering Compliance checks if the Service offering verifiable credential is compliant with all technical Gaia-x requirements.  

[Here](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/service/#service-offering) is the reference of service offering object : https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/service/#service-offering  

Like the table of the Legal Person Compliance rules, this one is about service offering rules provided by Gaia-X for the (located) service offering VC. Each of these rules MUST be validate.

| Rule name | Description |
| --------- | ----------- |
| CSR-01_checkJsonLd | Check json-ld validity |
| CSR-02_checkSchema | Check gaia-x [schemas](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/blob/develop/single-point-of-truth/yaml/gax-trust-framework/gax-service/service-offering.yaml) gax-core and gax-service used and verified |
| CSR-03_checkSignature | Check signatures validity as defined by the TrustFramework |
| CSR-04_checkHttpCode | All URI provided MUST be valide and resolvable |
| CSR-05_checkVCProvider | Legal Person MUST have a "complianceCredential" VC |
| CSR-06_checkDid | All DID are resolvable using a did resolver. Did example : did.web, did:key, did:ebsi etc  <br>If a did:web contains a [path](https://www.w3.org/TR/did-core/#dfn-did-paths), [fragment](https://www.w3.org/TR/did-core/#dfn-did-fragments) and/or [query](https://www.w3.org/TR/did-core/#dfn-did-queries) the resolver will ignore these informations |
| CSR-07_checkTracability | The keypair used to sign the Data Resource claims MUST be traceable to the producedBy participant of the Data Resource. [cf. TrustFramework](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/resource/#data-resource) |
| CSR-08_checkContainsPII | If containsPIIis true, attributes related to the Legitimate Processing of Information related to PII become mandatory. [cf. TrustFramework](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/resource/#data-resource) |

## Labelling

Gaia-X Labelling is the set of rules that defines the minimum baseline to be part of the Gaia-X Ecosystem. Those rules ensure a common governance and the basic levels of interoperability across individual ecosystems while preserving users from being in full control of their decisions. In other words, the Gaia-X Ecosystem is the virtual set of Participants and Service Offerings following the Gaia-X requirements from the Gaia-X Trust Framework. Gaia-X labelling is thus defined as the process of examining and validating the criterias that are met or not.

Each label is defined by a set of criterion.
List of criterion to validate labels : https://gitlab.com/gaia-x/policy-rules-committee/label-document/-/blob/master/docs/policy_rules_labeling_document.md

| Label level | Description | link |
| --------- | ----------- | ---------- |
| 1 | Data protection, transparency, security, portability, and flexibility are guaranteed in line with the rules defined in the Gaia-X Policy Rules Document and the basic set of technical requirements derived from the Gaia-X Architecture Document. For cybersecurity, with the minimum requirement being to meet ENISA’s European Cybersecurity Scheme - Basic Level. | [Label 1](https://gaia-x.community/participant/020dc2af9516b2af509e349f577c06aa681ef9799810faf79bb27de791af0e0d/vc/ee73e5504ca6d9e79fe7726fc31eca9b609f45ed761debfee60cca9257b9d0ec/data.json) |
| 2 | This advanced Label Level 2 extends the basic requirements from Level 1 and reflects a higher level of security, transparency of applicable legal rules and potential dependencies. The option of a service location in Europe MUST be provided to the consumer. Regarding cybersecurity, the minimum requirement will be to meet ENISA European Cybersecurity Scheme - Substantial Level. | [Label 2](https://gaia-x.community/participant/020dc2af9516b2af509e349f577c06aa681ef9799810faf79bb27de791af0e0d/vc/8a9bae461da3b6945ece65b342fe14093509eda4494f5b5116a14d64736211b5/data.json) |
| 3 | This level targets the highest standards for data protection, security, transparency, portability, and flexibility, as well as European control. It extends the requirements of Levels 1 and 2, with criteria that ensure immunity to non-European access and a strong degree of control over vendor lock-in. A service location in Europe is mandatory. For cybersecurity, the minimum requirement will be to meet ENISA’s European Cybersecurity Scheme - High Level. | [Label 3](https://gaia-x.community/participant/020dc2af9516b2af509e349f577c06aa681ef9799810faf79bb27de791af0e0d/vc/3dddebee83fef07bc13c24537544c1ce1df1875a01a281413551c1e2b743611a/data.json) |

More details on labelling: https://gaia-x.eu/wp-content/uploads/files/2021-11/Gaia-X%20Labelling%20Framework_0.pdf

### Contractual framework
| Categories | Criterion | Verification Process |
| --------- | ----------- | ----------- |
| Contractual governance | 1.1.x | Self-assessment through the trust framework |
| General material requirements and transparency | 1.2.x | Self-assessment through the trust framework |
| Technical compliance requirements | 1.3.x | Self-assessment through the trust framework |
  
### Data Protection
| Categories | Criterion | Verification Process |
| --------- | ----------- | ----------- |
| General | 2.1.x | Label 1: self-verified through internal audit according to an approved CoC/certification scheme and signed Gaia-X self-declaration<br />Label 2 / Label 3: evaluation by monitoring or third party; certification: inspection/verification/validation based on audit by CAB |
| GDPR Art. 28 | 2.2.x | Label 1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion. <br />Label 2 / Label 3: In case of a Code of Conduct (Art. 40 GDPR): assessment process as defined by the respective Code of Conduct / accredited monitoring body; In case of a Certification (Art. 43 GDPR): assessment process as defined by the respective Certification / accredited certification body. |
| GDPR Art. 26 | 2.3.x | Label 1: legally binding statement towards Gaia-X via the Trust Framework to comply with the Gaia-X Labelling criterion.<br />Label 2 / Label 3: In case of a Code of Conduct (Art. 40 GDPR): assessment process as defined by the respective Code of Conduct / accredited monitoring body; In case of a Certification (Art. 43 GDPR): assessment process as defined by the respective Certification / accredited certification body. |
  
### Cybersecurity
| Categories | Criterion | Verification Process |
| --------- | ----------- | ----------- |
| Cybersecurity | 3.1.x | Label 1: internal audit; externally confirmed to be following recognized standards and/or good practices<br />Label 2: onsite assessment following assessment process according to the respective standards<br />Label 3: According to process for EUCS Level High ; ad interim: see Label Level 2 |

### Portability
| Categories | Criterion | Verification Process |
| --------- | ----------- | ----------- |
| Switching and porting of Customer Data | 4.1.x | Label 1 & Label 2: self-verified through internal audit and signed Gaia-X Self-Declaration<br />Label 3: SWIPO self-declaration |

### European Control
| Categories | Criterion | Verification Process |
| --------- | ----------- | ----------- |
| Processing and storing of Customer Data in EU/EEA | 5.1.1 | Label 2 : Self declaration until an external entity is accredited by the Gaia-x European Association for Data and Cloud AISBL |
| Processing and storing of Customer Data in EU/EEA | 5.1.2 to 5.1.7 | Label 3 : Self declaration until an external entity is accredited by the Gaia-x European Association for Data and Cloud AISBL |
| Access to Customer Data | 5.2.1 | Gaia-X Self-Declaration |

To see more details on Crierion: https://docs.gaia-x.eu/policy-rules-committee/policy-rules-labelling/22.11/policy_rules_labeling_document/#gaia-x-policy-rules-and-labelling-criteria-for-providers

# Objects 

## Class Diagram 

```{mermaid}
classDiagram
  class SelfAssessedComplianceCriteriaClaim {
    hasServiceOffering: LocatedServiceOffering
    grantsComplianceCriteriaCombination: List~ComplianceCriteria~
  }
  class ComplianceCriterion {
    hasName: xsd:string
    hasDescription: xsd:string
    hasLevel: xsd:string
    hasCategory: xsd:string
    canBeSelfAssessed: xsd:boolean
  }
  class ComplianceLabel {
    hasName: xsd:string
    hasDescription: xsd:string
    hasLevel: xsd:string
    hasRequiredComplianceCriteriaCombination: List~ComplianceCriterion~
  }
  class LocatedServiceOffering {
  }
  class ComplianceCertificateClaim {
    hasComplianceCertificationScheme: ComplianceCertificationScheme
    hasServiceOffering: LocatedServiceOffering
  }
  class ComplianceCertificationScheme {
    hasComplianceReference: ComplianceReference
    grantsComplianceCriteriaCombination: List~ComplianceCriterion~
  }

  class ThirdPartyComplianceCertificateClaim {
    hasComplianceCertificationScheme: ThirdPartyComplianceCertificationScheme
    hasServiceOffering: LocatedServiceOffering
    hasConformityAssessmentBody: ConformityAssessmentBody
  }
  class ThirdPartyComplianceCertificationScheme {
    hasConformityAssessmentBodies: List~ConformityAssessmentBody~
    hasComplianceReference: ComplianceReference
    grantsComplianceCriteriaCombination: List~ComplianceCriterion~
  }

  class ComplianceReference {
    hasReferenceUrl: xsd:anyURI
    hasSha256: xsd:string
    hasComplianceReferenceTitle: xsd:string
    hasDescription: xsd:string
    hasComplianceReferenceManager: ComplianceReferenceManager
    hasVersion: xsd:string
    cRValidFrom: xsd:dateTime
    cRValidUntil: xsd:dateTime
  }
  class Participant {
  }
  class ComplianceReferenceManager {
    hasComplianceReferences: List~ComplianceReference~
  }
  class ConformityAssessmentBody {
    canCertifyThirdPartyComplianceCertificationScheme: List~ThirdPartyComplianceCertificationScheme~
  }
  class GrantedLabel {
    vc: xsd:string
    label: Label
    proof-vc: sec:proof
  }
  class ComplianceCredential {
    hash: wsd:string
  }

  SelfAssessedComplianceCriteriaClaim "1" -- "*" LocatedServiceOffering
  SelfAssessedComplianceCriteriaClaim "1" -- "*" ComplianceCriterion
  ComplianceCriterion "1" -- "*" ComplianceLabel
  LocatedServiceOffering "1" -- "*" ComplianceCertificateClaim
  ComplianceCriterion "1" -- "*" ComplianceCertificationScheme
  ComplianceCertificateClaim "1" -- "1" ComplianceCertificationScheme
  LocatedServiceOffering "1" -- "*" ThirdPartyComplianceCertificateClaim
  ComplianceCriterion "1" -- "*" ThirdPartyComplianceCertificationScheme
  ThirdPartyComplianceCertificateClaim "1" -- "1" ThirdPartyComplianceCertificationScheme
  ComplianceCertificationScheme "*" -- "1" ComplianceReference
  ThirdPartyComplianceCertificationScheme "*" -- "1" ComplianceReference
  ThirdPartyComplianceCertificationScheme "*" -- "*" ConformityAssessmentBody
  ComplianceReference "*" -- "1" ComplianceReferenceManager
  ConformityAssessmentBody <|-- Participant
  ComplianceReferenceManager <|-- Participant
  GrantedLabel "1" -- "1" LocatedServiceOffering
  ComplianceCredential "1" -- "1" LocatedServiceOffering
```
Class definitions: 
|Class|Purpose|VC Issued by|CASCO Terms|
|--|--|--|--| 
| SelfAssessedComplianceCriteriaClaim | (declaration of gaia-x labels criteria): group self assessed criteria labels for a provider to request a label for a service offering that may be linked to a location. This self assessed is done by a provider | | declaration about a criteria |
| ComplianceCriterion | it's a criterion used by ComplianceCertificationScheme or SelfAssessedComplianceCriteriaClaim or ThirdPartyComplianceCertificationScheme. </br> A SelfAssessedComplianceCriteriaClaim can receive only criterion allowing self assessed. | | requirement |
| ComplianceLabel | Label with a name, description and the list of criteria for that label. This object is used to define the Gaia-x label associated to the service in the Gaia-x Labelling Verifiable Credential.  | | conformity assessment scheme |
| LocatedServiceOffering | Service Offering in one Location. Most of claims are valid only for one and only one location.  | | object / service offering | 
| ComplianceCertificateClaim (declaration of a certification) | self assessed claim for a provider to request a label for a service offering that may be linked to a location. This  claim relates to criteria statisfying one or more Gaia-x label criteria. The link between labels and criteria is done by ComplianceCertificationScheme. | |  declaration about a schema | 
| ThirdPartyComplianceCertificateClaim (certification by a CAB) | same as  ComplianceCertificateClaim but signed by a CAB |  CAB | certification about a schema | 
| ComplianceCertificationScheme | The scheme expose a norm that relates to the claims and list labeling criteria validated by this norm | | conformity assessment scheme |
| ThirdPartyComplianceCertificationScheme | The scheme expose a norm that relates to the claims and list labeling criteria validated by this norm with a list of all CAB allowed to validate this norm (and by extention claims related to this scheme). | | conformity assessment scheme |
| ComplianceReference | This object represents a set of documentation norms and standards, services must comply | | scope of attestation |
| ComplianceReferenceManager | This object is a Participant in charge of emitting  ComplianceReference Verifiable Crendential | | CAB |
| ConformityAssessmentBody | A conformity assessment body is trusted by Trust Anchor to assert a norm for a Service Offerings instead of the issuer of this norm. (the right for a CAB to sign the norm instead of issuer is referenced by the Trust Anchor List). | | CAB | 
| GrantedLabel | The Granted Label object is generated by the labelling service and plays a crucial role in the Gaia-X ecosystem. The Gaia-X Label assigned to a specific service, ensure that it meets the necessary criteria and requirements. The labelling service helps maintain a high level of trust within the Gaia-X ecosystem. | | attestation about a schema | 
| ComplianceCredential | The Compliance object is generated by the Compliance Service and plays a crucial role in the Gaia-x ecosystem and ensure that it meets the necessary criteria and requirements. The Compliance Service helps maintain a high level of interoperability and conformity to Gaia-x Standards | Federation (compliance service) | attestation about a schema|

## Criterion 

### ComplianceLabel 
| Name | Type | Note |
| --------- | ----------- | ----------- |
| hasName | string | Name of the Gaia-x Label |
| hasDescription | string |  |
| hasLevel | string | Label level (1, 2 or 3) |
| hasRequiredComplianceCriteriaCombination | List\<ComplianceCriterion\> | List of criteria required for label validation |

Refers to a label defined in the Gaia-X Labelling Framework.

To complete a label, you MUST obtain or declare all the **Compliance Criterion** found in "**hasRequiredComplianceCriteriaCombination**".

A Criterion is defined by its "**ComplianceCriterion**" object.
The "**canBeSelfAssessed**" attribute allows to know if criteria can be found in the wizard questionnaire.

```json
{
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://w3id.org/security/suites/jws-2020/v1",
        "https://schemas.abc-federation.gaia-x.community/wip/contexts/for-credentials.json"
    ],
    "@type": [
        "VerifiableCredential",
        "ComplianceLabel"
    ],
    "@id": "did:web:gaia-x.community:participant:020dc2af9516b2af509e349f577c06aa681ef9799810faf79bb27de791af0e0d/vc/ee73e5504ca6d9e79fe7726fc31eca9b609f45ed761debfee60cca9257b9d0ec/data.json",
    "issuer": "did:web:gaia-x.community",
    "credentialSubject": {
        "@context": {
            "gx-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
            "xsd": "http://www.w3.org/2001/XMLSchema#"
        },
        "@id": "did:web:gaia-x.community/compliance-label/87cc15aed7e1e9f392a549a532ae6006f08f8fa1fe173f326efbf11dbcddf9b0/data.json",
        "@type": "gx-compliance:ComplianceLabel",
        "gx-compliance:hasName": {
            "@type": "xsd:string",
            "@value": "Gaia-X label Level 1"
        },
        "dct:hasDescription": {
            "@type": "xsd:string",
            "@value": "Gaia-X label Level 1"
        },
        "gx-compliance:hasLevel": {
            "@type": "xsd:string",
            "@value": "Level 1"
        },
        "gx-compliance:hasRequiredCriteria": [
            {
                "@type": "gx-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/b628c61b61bf5ea54c76d97913f30a7b249cac68df2ccc428593838cc4ddebe8/data.json"
            },
            {
                "@type": "gx-compliance:ComplianceCriterion",
                "@id": "did:web:gaia-x.community/compliance-criterion/ea307320071d2c97ba1fc264ac2b57fd571a30fd4bc08b85139ca3256a726b97/data.json"
            } ...
        ]
    },
    "issuanceDate": "2023-01-26T18:28:47.196823+00:00",
    "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:gaia-x.community",
        "created": "2023-01-26T18:28:47.196823+00:00",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..o080TIkyTY2ARaA7faRuMhkHxQo9KNcfGFOW7wi6zbnaXmGFklOskUJZDrQwg5NwzAByMX80HNqK1onf-Gc7n5nfpUg_kzNWH8zCKr82Yv24dZk9F23SJfv0GmrWOn6aNzkWE5Nghmc6_Qm1p_uGzm-bJssxruM5cg0cIyzK08uvJ0HqSQpgjoj5g_jPgNEcuccNB-w86g3msRmmt7mkySNeuE_GrFAPY4QdHz_B1txvZmXPUrRPC1u9Ih1J3PfW-OngSA5K2ery1GviKk7sqQCnnN8uKQAxrVS_ASTCrrEzuOXu4Pqacezpu2lKGvb0o_Fq-WST-M6vfl0nm6t9ZQ"
    }
}
```
### ComplianceCriterion
| Name | Type | Note |
| --------- | ----------- | ----------- |
| hasName | string | Criterion name |
| hasDescription | string | Criterion Description  |
| hasLevel | string | Gaia-x Level corresponding to this criterion |
| hasCategory | string | Criterion category |
| canBeSelfAssessed | boolean | Define whether this criterion should be included the Wizard's selfAssessment questionnaire |

Refers to a criterion defined in the Gaia-X Trust Framework or Labelling Criteria.

URL to find all the criterias : https://federated-catalogue-api.abc-federation.dev.gaiax.ovh/api/compliance_criterions

Example: [Criterion 1 Label 1](https://gaia-x.community/compliance-criterion/b628c61b61bf5ea54c76d97913f30a7b249cac68df2ccc428593838cc4ddebe8/data.json)

```json
{
    "@context": {
        "gx-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
        "xsd": "http://www.w3.org/2001/XMLSchema#"
    },
    "@id": "did:web:gaia-x.community/compliance-criterion/b628c61b61bf5ea54c76d97913f30a7b249cac68df2ccc428593838cc4ddebe8/data.json",
    "@type": "gax-compliance:ComplianceCriterion",
    "gx-compliance:hasName": {
        "@type": "xsd:string",
        "@value": "Criterion 1"
    },
    "gx-compliance:hasDescription": {
        "@type": "xsd:string",
        "@value": "The provider shall offer the ability to establish a legally binding act. This legally binding act shall be documented"
    },
    "gx-compliance:hasLevel": {
        "@type": "xsd:string",
        "@value": "Level 1"
    },
    "gx-compliance:hasCategory": {
        "@type": "xsd:string",
        "@value": "Contractual governance"
    },
    "gx-compliance:canBeSelfAssessed": {
        "@type": "xsd:boolean",
        "@value": true
    }
}
```

## Self-assessed claims

Criterias validated by the answers to the questionnaire : 1.1.1 to 1.3.5 and 5.1.1 to 5.2.1

### SelfAssessedComplianceCriteriaClaim 
| Name | Type | Note |
| --------- | ----------- | ----------- |
| hasServiceOffering | ServiceOffering | URL to the linked Service Offering |
| grantsComplianceCriteriaCombination | List\<ComplianceCriterion\> | List of selfAssessed criteria |

```json 
{
   "@context":[
      "https://www.w3.org/2018/credentials/v1"
   ],
   "@id":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/077849c113d60b532c273d132c0443e0c8604a71157ae1348e42e68153971e5e/data.json",
   "type":[
      "VerifiableCredential",
      "SelfAssessedComplianceCriteriaCredential"
   ],
   "issuer":"did:web:dufourstorage.provider.gaia-x.community",
   "expirationDate":"2023-06-21T15:31:47.87+02:00",
   "credentialSubject":{
      "hasServiceOffering": "",
      "grantsComplianceCriteriaCombination":[
         "https://gaia-x.community/compliance-criterion/b628c61b61bf5ea54c76d97913f30a7b249cac68df2ccc428593838cc4ddebe8/data.json",
         "https://gaia-x.community/compliance-criterion/bc3a08b25f3e84f6e0bbc06e03b038cc04c9d3eee2a654d956e98f6360e6baa0/data.json"
      ]
   }
}
```

## Self-declared claims

The self-declared claims come from certifications that are self-signed by the provider and do not have any corresponding claims from a Certification Authority (CAB) (see Claims reference to CAB's Claims).
Notarization associates each claim with its corresponding criterion and stores it in the "**grantsComplianceCriteria**" field. The criteria in this field MUST be combined with those in the same field in the schemas coming from the **ThirdPartyComplianceCertificationScheme**.

### ComplianceCertificateClaim
| Name | Type |
| --------- | ----------- |
| hasComplianceCertificationScheme | ComplianceCertificationScheme |
| hasServiceOffering | ServiceOffering | 

This object claims a service offering has been certified for a compliance.

```json
{
    "@context": [
        "https://www.w3.org/2018/credentials/v1"
    ],
    "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/compliance-certificate-claim/01e0c93709bc28e965d0241d542c129593a0caccc4d54483176cbf74d59230e0/data.json",
    "issuer": "did:web:abc-federation.gaia-x.community",
    "@type": [
        "VerifiableCredential",
        "ComplianceCertificateClaim"
    ],
    "credentialSubject": {
        "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/compliance-certificate-claim/01e0c93709bc28e965d0241d542c129593a0caccc4d54483176cbf74d59230e0/data.json",
        "@type": "ThirdPartyComplianceCertificateClaim",
        "@context": {
            "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
            "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#",
            "gax-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#"
        },
        "gax-compliance:hasComplianceCertificationScheme": {
            "@type": "gax-compliance:ComplianceCertificationScheme",
            "@value": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/conformity-assessment-bodies/0dc49d6198f527971932f3de36953978605821d4b7a0c535f6fb6ebda6daaa5a/data.json"
        },
        "gax-compliance:hasLocatedServiceOffering": {
            "@type": "gax-service:LocatedServiceOffering",
            "@value": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/located-service-offering/3932578eed035590e7386d98268d5cc94ea10d38ffad2202827db1ae46a3c5de/data.json"
        }
    },
    "issuanceDate": "2023-03-08T10:05:40.744775+00:00",
    "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:abc-federation.gaia-x.community",
        "created": "2023-03-08T10:05:40.744775+00:00",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..ZA_TC4YX-8cwyFXC7cXmIN7LkEp6zbqebzfp2pXE6olSiajo_qQH0gdvBAi_6Gpe2jVenD0UctBRqpL4G_b6bU6Rn4FMTgcwK1unwOOcdaL_QPm2i88cnjusGOf9GE_ULKIntgvuLYvdtWJerrWnQmty-KIHtAOxFPACfsq-lBe4eaIfTk0wD3rCXb_vve0UPOkR5y4vxxXMfc4HwmXuPeKE8JZmvHwbbjbd9ZmSG4Ce6x-Py22kQSM9m04nfUyvh05FTaHHpdzYriOGIPG_WoBj00k0Yp_Sl70JUJyGwbvOpQAhD8cLWYrF5QVsQ2DS6rFRy47rcQBXEGC7K2g6rQ"
    }
}
```

### ComplianceCertificationScheme 
| Name | Type | Note |
| --------- | ----------- | ----------- |
| hasComplianceReference | ComplianceReference | --- |
| grantsComplianceCriteria | List\<ComplianceCriterion\> | List of criteria validated by the claim linked to this scheme |

This object declares a way to certify a Service Offering from a Participant has a ComplianceReference. 

```json
{
    "@context": {
        "gx-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#",
        "gx-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
        "xsd": "http://www.w3.org/2001/XMLSchema#"
    },
    "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/compliance-certification-scheme/0c8f60212eb57bb4d64e28831c36acd0c691bcfe52525c85780ffe65e1a1bf30/data.json",
    "@type": "ComplianceCertificationScheme",
    "gx-compliance:hasComplianceReference": {
        "@type": "gx-compliance:ComplianceReference",
        "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/compliance-reference/a64d4df83757fed081be3ce2bdcaa8b362b341c64cb7ad97b935cc6a8c54dc25/data.json"
    },
    "gx-compliance:grantsComplianceCriteria": [
        {
            "@type": "gx-compliance:ComplianceCriterion",
            "@id": "did:web:gaia-x.community/compliance-criterion/85044fd562ef04649b9d5530e7f4ef248ae6ab81b3c9fa1f4fcd3847ce1c94f1/data.json"
        },
        {
            "@type": "gx-compliance:ComplianceCriterion",
            "@id": "did:web:gaia-x.community/compliance-criterion/54f1389a18b0e1699e33208db67a17b81e086aff172908fb315c8a8080745695/data.json"
        }
    ]
}
```
### ComplianceCertificateCredential

| Name | Type |
| ----------- | ----------- |
| isValid | Boolean |
| hasComplianceCertificateClaim | ComplianceCertificateClaim |

Is a VerifiableCredential self-signed by a Provider in the case of self-assessed labelling criterion.

```json
{
    "@context": {
        "gx-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
        "gx-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#"
    },
    "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/compliance-certificate-credential/1ad717bb4f00327b511aeb47794e1fa3d98c22ad46cc724882712be745237bc1/data.json",
    "@type": [
        "VerifiableCredential"
    ],
    "issuer": "did:web:abc-federation.gaia-x.community",
    "credentialSubject": {
        "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/third-party-compliance-certificate-claim/da593713a16f42bb0e0753bc14c770fccb609e5925c57306c9e1861697fd76a8/data.json",
        "@type": "ComplianceCertificateCredential",
        "isValid": {    
            "@value": "True"
            "@type": "xsd:boolean"
        }
        "hasComplianceCertificateClaim":{
            "@value": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/compliance-certificate-claim/01e0c93709bc28e965d0241d542c129593a0caccc4d54483176cbf74d59230e0/data.json"
            "@type": "ComplianceCertificateClaim"
        }
    },
    "issuanceDate": "2022-11-03T16:54:55.544202+00:00",
    "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:abc-federation.gaia-x.community",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..jMg2O46MsorM9pMfDEiSDar1z_u-Vpd5-TeALH6z_Jymdipz7hlYiAYy13PUNXGzm2_BibzAeKZT2oR8aUK3avAbY7DeN6Hz6xZ4CJ9LcuYUqZFm9eG2d1c8-wqyODcwYJJ1MAMnirPQE9uxgCI7AUnaoFt0MEsv1myWOEOVhg90dkpeiu--cLoXl6l34Bofiy2I5gb2ZxzD9USRsTe3Fvr3yRvMyH3NQdxLFgqkIPmgvLczV_v_69pY7ZRiCeRGrUbyTcejkX2CGeBLtWmjrU51x428fyJPFRdNiOlf1kbXTdAGFwu3qnvO_A8QNKTaDjyb_6wHvVAWJYiJjuc72g"
    }
}
```
## Claims reference to CAB's Claims

Those are claims signed by a provider, it deals with the claim(s) issued and signed by a CAB. The claims signed by a trusted third party validate the higher-level criteria.

### ThirdPartyComplianceCertificateClaim
| Name | Type |
| --------- | ----------- |
| hasComplianceCertificationScheme | ThirdPartyComplianceCertificationScheme |
| hasServiceOffering | ServiceOffering |
| hasConformityAssessmentBody | ConformityAssessmentBody |

In addition to the ComplianceCertificateClaim, a reference to the **conformity assessment bodies** is provided.

```json
{
    "@context": [
        "https://www.w3.org/2018/credentials/v1"
    ],
    "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/third-party-compliance-certificate-claim/01e0c93709bc28e965d0241d542c129593a0caccc4d54483176cbf74d59230e0/data.json",
    "issuer": "did:web:abc-federation.gaia-x.community",
    "@type": [
        "VerifiableCredential",
        "ThirdPartyComplianceCertificateClaim"
    ],
    "credentialSubject": {
        "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/third-party-compliance-certificate-claim/01e0c93709bc28e965d0241d542c129593a0caccc4d54483176cbf74d59230e0/data.json",
        "@type": "ThirdPartyComplianceCertificateClaim",
        "@context": {
            "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
            "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#",
            "gax-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#"
        },
        "gax-compliance:hasComplianceCertificationScheme": {
            "@type": "gax-compliance:ComplianceCertificationScheme",
            "@value": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/conformity-assessment-bodies/0dc49d6198f527971932f3de36953978605821d4b7a0c535f6fb6ebda6daaa5a/data.json"
        },
        "gax-compliance:hasLocatedServiceOffering": {
            "@type": "gax-service:LocatedServiceOffering",
            "@value": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/located-service-offering/3932578eed035590e7386d98268d5cc94ea10d38ffad2202827db1ae46a3c5de/data.json"
        },
        "gax-compliance:hasConformityAssessmentBody": {
            "@type": "gax-participant:ConformityAssessmentBody",
            "@value": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/conformity-assessment-bodies/0dc49d6198f527971932f3de36953978605821d4b7a0c535f6fb6ebda6daaa5a/data.json"
        }
    },
    "issuanceDate": "2023-03-08T10:05:40.744775+00:00",
    "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:abc-federation.gaia-x.community",
        "created": "2023-03-08T10:05:40.744775+00:00",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..ZA_TC4YX-8cwyFXC7cXmIN7LkEp6zbqebzfp2pXE6olSiajo_qQH0gdvBAi_6Gpe2jVenD0UctBRqpL4G_b6bU6Rn4FMTgcwK1unwOOcdaL_QPm2i88cnjusGOf9GE_ULKIntgvuLYvdtWJerrWnQmty-KIHtAOxFPACfsq-lBe4eaIfTk0wD3rCXb_vve0UPOkR5y4vxxXMfc4HwmXuPeKE8JZmvHwbbjbd9ZmSG4Ce6x-Py22kQSM9m04nfUyvh05FTaHHpdzYriOGIPG_WoBj00k0Yp_Sl70JUJyGwbvOpQAhD8cLWYrF5QVsQ2DS6rFRy47rcQBXEGC7K2g6rQ"
    }
}
```
### ThirdPartyComplianceCertificationScheme
| Name | Type | Note |
| --------- | ----------- | ----------- |
| hasComplianceAssessmentBodies | List\<ConformityAssessmentBody\> | ComplianceAssessmentBodies set up verify the truth of a claim |
| hasComplianceReference | ComplianceReference | --- |
| grantsComplianceCriteria | List\<ComplianceCriterion\> | List of criteria validated by the claim linked to this scheme |

This object is a concrete certification scheme that allows an non-ordered list of ConformityAssessmentBody roles to claim if a service offering provided by a Provider is certified.

A ThirdPartyComplianceCertificationScheme instance is signed by a ComplianceReferenceManager.

```json
{
    "@context": {
        "gx-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#",
        "gx-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
        "xsd": "http://www.w3.org/2001/XMLSchema#"
    },
    "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/third-party-compliance-certification-scheme/0c8f60212eb57bb4d64e28831c36acd0c691bcfe52525c85780ffe65e1a1bf30/data.json",
    "@type": "gx-compliance:ThirdPartyComplianceCertificationScheme",
    "gx-compliance:hasComplianceAssessmentBodies": [
        {
            "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/conformity-assessment-bodies/0dc49d6198f527971932f3de36953978605821d4b7a0c535f6fb6ebda6daaa5a/data.json",
            "@type": "gx-compliance:ConformityAssessmentBody"
        },
        {
            "@id": "did:web:sgs.auditor.gaia-x.community:participant:d54b2245a2156fc724e804e3ea65e8cdecbd375380668497fcc090d3537c4d15/data.json",
            "@type": "gx-compliance:ConformityAssessmentBody"
        }
    ],
    "gx-compliance:hasComplianceReference": {
        "@type": "gx-compliance:ComplianceReference",
        "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/compliance-reference/a64d4df83757fed081be3ce2bdcaa8b362b341c64cb7ad97b935cc6a8c54dc25/data.json"
    },
    "gx-compliance:grantsComplianceCriteria": [
        {
            "@type": "gx-compliance:ComplianceCriterion",
            "@id": "did:web:gaia-x.community/compliance-criterion/85044fd562ef04649b9d5530e7f4ef248ae6ab81b3c9fa1f4fcd3847ce1c94f1/data.json"
        },
        {
            "@type": "gx-compliance:ComplianceCriterion",
            "@id": "did:web:gaia-x.community/compliance-criterion/54f1389a18b0e1699e33208db67a17b81e086aff172908fb315c8a8080745695/data.json"
        }
    ]
}
```

### ThirdPartyComplianceCertificateCredential

| Name | Type |
| ----------- | ----------- |
| isValid | Boolean |
| hasThirdPartyComplianceCertificateClaim | ThirdPartyComplianceCertificateClaim |

Is a VerifiableCredential signed by a ConformityAssessmentBody in the case of audited criterion.
```json
{
    "@context": {
        "gx-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
        "gx-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#"
    },
    "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/third-party-compliance-certificate-credential/1ad717bb4f00327b511aeb47794e1fa3d98c22ad46cc724882712be745237bc1/data.json",
    "@type": [
        "VerifiableCredential",
        "ThirdPartyComplianceCertificateCredential"
    ],
    "issuer": "did:web:abc-federation.gaia-x.community",
    "credentialSubject": {
        "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/third-party-compliance-certificate-claim/da593713a16f42bb0e0753bc14c770fccb609e5925c57306c9e1861697fd76a8/data.json",
        "@type": "gx-compliance:ThirdPartyComplianceCertificateClaim",
        "isValid": {
            "@value": "True"
        }
    },
    "issuanceDate": "2022-11-03T16:54:55.544202+00:00",
    "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:abc-federation.gaia-x.community",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..jMg2O46MsorM9pMfDEiSDar1z_u-Vpd5-TeALH6z_Jymdipz7hlYiAYy13PUNXGzm2_BibzAeKZT2oR8aUK3avAbY7DeN6Hz6xZ4CJ9LcuYUqZFm9eG2d1c8-wqyODcwYJJ1MAMnirPQE9uxgCI7AUnaoFt0MEsv1myWOEOVhg90dkpeiu--cLoXl6l34Bofiy2I5gb2ZxzD9USRsTe3Fvr3yRvMyH3NQdxLFgqkIPmgvLczV_v_69pY7ZRiCeRGrUbyTcejkX2CGeBLtWmjrU51x428fyJPFRdNiOlf1kbXTdAGFwu3qnvO_A8QNKTaDjyb_6wHvVAWJYiJjuc72g"
    }
}
```
## Self descriptions for compliance

### ComplianceReferenceManager
| Name | Type | Note |
| ----------- | ----------- | ----------- |
| hasDidWeb | string | --- |
| registrationNumber | string | Country’s registration number, which identifies one specific entity. |
| legalAddress | Address | Physical location of legal registration |
| headquarterAddress | Address | Physical location of the headquarters |
| termsAndConditions | TermsAndConditions | TermsAndConditions Objects of the Generic Terms and Conditions for Gaia-X Ecosystem |
| hasComplianceReferences | List\<ComplianceReferences\> | --- |

This actor is a Participant inside a given Gaia-X ecosystem in charge of emitting VCs for ComplianceReference and subclasses of ComplianceCertificationScheme.

Several ComplianceReferenceManager can be involved in the same Gaia-X ecosystem. N.B.: A ComplianceReferenceManager has authority only on the ComplianceReference instances it has created as well as the ComplianceCertificationScheme referencing the ComplianceReference.

Each ComplianceReferenceManager MUST be validated and approved by the **Gaia-X Trust Framework Service** (ComplianceReferenceManager VerifiableCredential is signed by https://compliance.gaia-x.eu/).

To make the bootstrap of compliance features easier, the default ComplianceReferenceManager is the **Gaia-X Trust Framework Service** itself.

```json
{
    "@context": {
        "xsd": "http://www.w3.org/2001/XMLSchema#",  
        "gx-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
        "gx-core": "https://schemas.abc-federation.gaia-x.community/wip/vocab/core#"
    },
    "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/data.json",
    "@type": "gx-participant:ComplianceReferenceManager",
    "gx-participant:hasDidWeb": {
        "@type": "xsd:string",
        "@value": "did:web:abc-federation.gaia-x.community:participant/did.json"
    },
    "gx-participant:registrationNumber": {
        "@type": "xsd:string",
        "@value": "1234567890"
    },
    "gx-participant:legalAddress": {
        "@type": "vcard:Address",
        "vcard:country-name": {
            "@type": "xsd:string",
            "@value": "BE"
        },
        "vcard:street-address": {
            "@type": "xsd:string",
            "@value": "Avenue des Arts 6-9"
        },
        "vcard:postal-code": {
            "@type": "xsd:string",
            "@value": "1210"
        },
        "vcard:locality": {
            "@type": "xsd:string",
            "@value": "Bruxelles"
        }
    },
    "gx-participant:headquarterAddress": {
        "@type": "vcard:Address",
        "vcard:country-name": {
            "@type": "xsd:string",
            "@value": "BE"
        },
        "vcard:street-address": {
            "@type": "xsd:string",
            "@value": "Avenue des Arts 6-9"
        },
        "vcard:postal-code": {
            "@type": "xsd:string",
            "@value": "1210"
        },
        "vcard:locality": {
            "@type": "xsd:string",
            "@value": "Bruxelles"
        }
    },
    "gx-participant:termsAndConditions": {
        "@type": "gx-core:TermsAndConditions",
        "gx-core:Value": {
            "@type": "xsd:anyURI",
            "@value": "https://abc-federation.gaia-x.community/terms"
        },
        "gx-core:hash": {
            "@type": "xsd:string",
            "@value": "e9639e3c4681ce85f852fbac48e2eeee5ba51296dbfec57c200d59b76237ab80"
        }
    },
    "gx-compliance:hasComplianceReferences": [
        {
            "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/compliance-reference/b99fce61774186fe31ad64f1ad1f66100244a14f3e0fb8edd57a6a202b27730e/data.json",
            "@type": "gx-compliance:ComplianceReference"
        },
        {
            "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/compliance-reference/e0423237acdcb2844f0db3c688353be38e04dc1078ce200a06d94f74f40e1641/data.json",
            "@type": "gx-compliance:ComplianceReference"
        }
    ]
}
```

### ComplianceReference
| Name | Type | Note |
| ----------- | ----------- | ----------- |
| hasReferenceUrl | string | URL of the compliance reference |
| hasSha256 | string | The SHA256 hash value of the compliance reference |
| hasComplianceReferenceTitle | string | Title of the compliance reference |
| hasComplianceReferenceManager | string | The name of the Compliance Reference Manager of the compliance reference |
| referenceType | string | --- |
| hasVersion | string | Version of the compliance reference |
| cRValidFrom | string | The date from which the compliance reference is valid |
| cRValidUntil | string | The date until which the compliance reference is valid |
| hasComplianceCertificationSchemes | string | The compliance certification schemes associated with the compliance reference |

This object represents a set of documentation norms and standards, services MUST comply with. For example: SecNumCloud.

A ComplianceReference is composed of mandatory attributes like: the URI of reference document related to the compliance, the title, description version and validity dates of the reference.

Each ComplianceReference is signed by a ComplianceReferenceManager. 
This relationship is indicated through:
* the `hasComplianceReferenceManager` attribute of the ComplianceReference
* the `issuer` attribute of the created ComplianceReference VerifiableCredential

```json
{
    "@context": {
        "gx-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
        "xsd": "http://www.w3.org/2001/XMLSchema#"
    },
    "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/compliance-reference/a64d4df83757fed081be3ce2bdcaa8b362b341c64cb7ad97b935cc6a8c54dc25/data.json",
    "@type": "gx-compliance:ComplianceReference",
    "gx-compliance:hasReferenceUrl": {
        "@type": "xsd:anyURI",
        "@value": "https://www.iso.org/fr/standard/54534.html"
    },
    "gx-compliance:hasSha256": {
        "@type": "xsd:string",
        "@value": "d44734012294061eae329d230971272b9f9a1e87209d3a223fa3b527550745dd"
    },
    "gx-compliance:hasComplianceReferenceTitle": {
        "@type": "xsd:string",
        "@value": "ISO 27001"
    },
    "gx-compliance:hasComplianceReferenceManager": {
        "@value": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/data.json",
        "@type": "gx-participant:ComplianceReferenceManager"
    },
    "gx-compliance:referenceType": {
        "@value": "Security",
        "@type": "xsd:string"
    },
    "gx-compliance:hasVersion": {
        "@value": "2013",
        "@type": "xsd:string"
    },
    "gx-compliance:cRValidFrom": {
        "@value": "2022-06-20T00:00:00",
        "@type": "xsd:dateTime"
    },
    "gx-compliance:cRValidUntil": {
        "@value": "2022-09-07T00:00:00",
        "@type": "xsd:dateTime"
    },
    "gx-compliance:hasComplianceCertificationSchemes": [
        {
            "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/third-party-compliance-certification-scheme/0c8f60212eb57bb4d64e28831c36acd0c691bcfe52525c85780ffe65e1a1bf30/data.json",
            "@type": "gx-compliance:ComplianceCertificationScheme"
        }
    ]
}
```
### ConformityAssessmentBody

| Name | Type | Note |
| ----------- | ----------- | ----------- |
| canCertifyThirdPartyComplianceCertificationScheme | List\<ThirdPartyComplianceCertificationScheme\> | The compliance certification schemes that the conformity assessment body can certify for third parties |
| hasThirdPartyComplianceCertificateClaim | List\<ThirdPartyComplianceCertificateClaim\> | The claims made by third parties about their compliance certification |
| hasThirdPartyComplianceCredential | List\<ThirdPartyComplianceCredential\> | The credentials held by third parties related to compliance certification |

`ConformityAssessmentBody` is a role assumed by a Participant inside a given Gaia-X ecosystem. 

Functionally, a **conformity assessment bodies** is trusted to assert Service Offerings provided by Providers have been certified for a given compliance in the case of a ThirdPartyComplianceCertificationScheme.

```json
{
    "@context": [
        "https://www.w3.org/2018/credentials/v1"
    ],
    "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/conformity-assessment-bodies/0dc49d6198f527971932f3de36953978605821d4b7a0c535f6fb6ebda6daaa5a/data.json",
    "issuer": "did:web:abc-federation.gaia-x.community",
    "@type": [
        "VerifiableCredential",
        "ConformityAssessmentBody"
    ],
    "credentialSubject": {
        "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/conformity-assessment-bodies/0dc49d6198f527971932f3de36953978605821d4b7a0c535f6fb6ebda6daaa5a/data.json",
        "@type": "ConformityAssessmentBody",
        "@context": {
            "gx-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
            "gx-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#"
        },
        "gx-compliance:canCertifyThirdPartyComplianceCertificationScheme": [
            {
                "@type": "gx-compliance:ThirdPartyComplianceCertificationScheme",
                "@value": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/third-party-compliance-certification-scheme/cb13b89ba43fbf91fd7a825c2269f6542159526f0415c9693ead6b25cd6c7699/data.json"
            }
        ],
        "gx-compliance:hasThirdPartyComplianceCertificateClaim": [
            {
                "@type": "gx-compliance:ThirdPartyComplianceCertificateClaim",
                "@value": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/third-party-compliance-certificate-claim/01e0c93709bc28e965d0241d542c129593a0caccc4d54483176cbf74d59230e0/data.json"
            }
        ],
        "gx-compliance:hasThirdPartyComplianceCredential": [
            {
                "@type": "gx-compliance:ThirdPartyComplianceCertificateCredential",
                "@value": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/third-party-compliance-certificate-credential/76edc899c48b7b3ee8026d501181fdc81a0dbc0bfe1ec328a8c17e8cdb99d0b2/data.json"
            }
        ]
    },
    "issuanceDate": "2023-03-08T10:05:40.643870+00:00",
    "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:abc-federation.gaia-x.community",
        "created": "2023-03-08T10:05:40.643870+00:00",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..fXPDsTKMRSntVW3YYnsDWLdx8PBrAp0lutWj_kTMdeh4By9LC4-PVOi1sjCtF8cDNYP5BmW0zt6mHOTz2SRej0nBAkd99i1EWF8_guy18YHJceJ-zHr06jXr72HszqwNuXUnzvsKBNkidL-eFx1SpEO03A-bXGQu5-7yZaJo7a2PhdcJQSNWgphncz86-pH_odFClhKPwTFOi01OFYJ-W07kVpPJugpKp1PEZLZsnXMYawIWc35bgsFkV1mbmUTput-6Z2NQl-C62JtR72HQiLMbL4dlky9f-g-Q4Z8aUZXzSoW6hXcLlS1mXj0AEBKkNnqbV1nCs1pOJP4Hb9FlIw"
    }
}
```

## Object from compliance (legal Person or Service Offering)

### ComplianceCredential

| Name | Type | Note |
| ----------- | ----------- | ----------- |
| hash | String | Targeted VC Content hashed |

The object obtained by passing the compliance for LegalPerson or the compliance for Service Offering | LocatedServiceOffering is a Verifiable Credential (VC). This credential is issued to validate that a participant, service, or location meets the necessary Gaia-X Trust Framework requirements. The VC for compliance is mandatory for being part of the Gaia-X ecosystem, ensuring that the participants and their offerings adhere to the established rules and maintain a high level of trust, interoperability, and compliance.

```json
{
    "@context": [
        "https://www.w3.org/2018/credentials/v1"
    ],
    "type": [
        "VerifiableCredential"
    ],
    "id": "https://compliance.abc-federation.dev.gaiax.ovh/0ebe2c2a-4130-4a76-a77b-12567490e5d4",
    "issuer": "did:web:compliance.abc-federation.dev.gaiax.ovh",
    "issuanceDate": "2023-04-11T09:47:52.289Z",
    "expirationDate": "2023-07-10T09:47:52.289Z",
    "credentialSubject": {
        "id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/aa2ab6e355fcdc6af45cce0f25e7843b1d7219b670544fac4c34dc1e3513eb6d/data.json",
        "hash": "e9307a84c46ea3e34ff7d5bac30494da400650c5e4256adc42f6afedf4487607",
        "type": "gx:complianceCredential"
    },
    "proof": {
        "type": "JsonWebSignature2020",
        "created": "2023-04-11T09:47:52.289Z",
        "proofPurpose": "assertionMethod",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..nTKPPnT5otJfqWSmtoTAjOOGDcvq9xN4099F6lTNiIkbKgylJctx8GxaH6rwvbwysx5SLeW-ZdoIU07DF7dK-w9LVjkJkpe4k1PmODardDnzyrih6tH3zExt-Ua7MojDDN-0BS1Fj31Zct71ERNwwpgcYv8kweZnbPGcjDDGAnZUUZzSVG5HKwHhj9Hmt_Nimc5m0lCA8SWNOBQYbtKfUFf73U_VIZV10DMdNrrROF53ejSCi5mG66o2q2vi07OnljQzf2Vnihao9g9WyNZkKNsqVfPoO6D6z6kBClvq6oiu8yDPrjQs5LFhNXJiD7dKa3KnYgNpps727kXJV5QKvw",
         "verificationMethod": "did:web:compliance.abc-federation.dev.gaiax.ovh"
    }
}
```

## Object from Labelling

### GrantedLabel
| Name | Type | Note |
| ----------- | ----------- | ----------- |
| vc | String | ID of the VC passing the labelling |
| label | Label | Label granted |
| proof-vc | Proof | The proof of the VC passing the labelling |

The GrantedLabel object is generated by the labelling service and plays a crucial role in the Gaia-X ecosystem. This object is used to validate and confirm the Gaia-X label assigned to a specific service, ensuring that it meets the necessary criteria and requirements. By validating the label, the labelling service helps maintain a high level of trust, interoperability, and compliance within the Gaia-X ecosystem.

```json
{
   "@context":[
      "https://www.w3.org/2018/credentials/v1"
   ],
   "id":"https://labelling.abc-federation.gaia-x.community/vc/857ebd4d-d261-4c9e-b085-cd2f614c1211",
   "type":[
      "VerifiableCredential",
      "GrantedLabel"
   ],
   "credentialSubject":{
      "id":"did:web:ovhcloud.provider.gaia-x.community:participant:4b2aceeb-cdb4-4bf6-9e6b-4b24612c5d72/granted-label/857ebd4d-d261-4c9e-b085-cd2f614c1211/data.json",
      "type":[
         "GrantedLabel"
      ],
      "vc":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/aa2ab6e355fcdc6af45cce0f25e7843b1d7219b670544fac4c34dc1e3513eb6d/data.json",
      "label":{
         "hasName":"Gaia-X label Level 1",
         "id":"did:web:gaia-x.community:participant:020dc2af9516b2af509e349f577c06aa681ef9799810faf79bb27de791af0e0d/vc/ee73e5504ca6d9e79fe7726fc31eca9b609f45ed761debfee60cca9257b9d0ec/data.json"
      },
      "proof-vc":{
         "type":"JsonWebSignature2020",
         "proofPurpose":"assertionMethod",
         "verificationMethod":"did:web:dufourstorage.provider.gaia-x.community",
         "jws":"eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..gY-SVGpXqJRsUa1SZW0PTLkqo2o9V08w1IjPKbr6f0AK6TLyoag9cOonlLNO2kNaV83pMDLv9Gak_DfJ93TPO6wTCiDUS1yFFeMUcBDct2QIc2m_t-a9CcpNQY3F6VMzC17Or3bpww7WHr676o6kFktI7dwnNl9uqFTzA9fdRm9tEEn2DkPhXvntFkQsS9u8X71DLp6WbUFj-NuMvcuUD0ZJFYAr6rTersdxR86foA0lMVUie6osgfC_G9Kwd5IdXzv8ttJa3QASuon-X7wwfz-cOVAN2gguG_RdjsKwVvTSoCxyObUoabNJXsZbiH4g83PIEka2JufrmtFqZRrAnQ"
      }
   },
   "expirationDate":"2023-07-10T00:00:00.000Z",
   "issuer":"did:web:abc-federation.gaia-x.community",
   "issuanceDate":"2023-04-11T09:49:30.265373+00:00",
   "proof":{
      "type":"JsonWebSignature2020",
      "proofPurpose":"assertionMethod",
      "verificationMethod":"did:web:abc-federation.gaia-x.community",
      "created":"2023-04-11T09:49:30.265373+00:00",
      "jws":"eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..bNK3FThSgD2nfgRTga7HOQFYS87N2dllar4B8397sztL0Z4nY4CiB4gYQxDyB0UQEWWQUTMiPKt7HjDHmdkdoWfGB4w67Vwr7wcW5dtdFPPxwT4TRqaP6E1s7gfZymOKs0Fn8CzX6ncVvB18DADMifA2ZGdjTEBtPLpBjC2O-aZbfxUWTIcXzrMkTbEE4kI8AKq7-IeAJRGtPUPP3OBQxsP9ro6heeB2Q3kM4_L6dI9a-2DNOy5NlEl9tK51gOGs4o2BCFbxrQ8FYNklOzC2T56HOlG-PdwyQu_cW2VDeLmAb-5pX40MBQB-JlARsgkWuvCFL3NWYLwMHIWwIcyjVQ"
   }
}
```
## Example of ThirdParty compliance certification

Initial setup

* A Participant CompRefManager is considered as a valid ComplianceReferenceManager by the Gaia-X Trust Framework service. Thus Participant CompRefManager has a ComplianceReferenceManager VC signed by https://compliance.gaia-x.eu/
* Participant ProviderA has been onboarded into the Ecosystem. Thus ProviderA has a Provider VC signed by https://compliance.gaia-x.eu/.
* Participant AUDITOR has been onboarded into the Ecosystem. Thus AUDITOR has a ConformityAssessmentBody VC signed by https://compliance.gaia-x.eu/.
* Participant ProviderA has registered a service offering name IAAS. This service if located in France. Thus a LocatedServiceOffering VC named IAAS-FR has been signed by https://compliance.gaia-x.eu/ .

A new Compliance ISO-27001 MUST be supported inside the ecosystem:

* Participant CompRefManager signs a ComplianceReference VC
* Participant CompRefManager evaluates the criticity of ISO-27001 compliance. It decides to rely on a network of conformity assessment bodies to trust assertion of certification compliance. ThirdPartyComplianceCertificationScheme is the only way to justify a service is ISO-27001 certified.
* Participant AUDITOR is considered as a valid conformity assessment bodies for ISO-27001 by the CompRefManager. Thus CompRefManager signs a ThirdPartyComplianceCertificationScheme VC which indicates Participant AUDITOR is allowed to claim ISO-27001 compliance.

Participant ProviderA wants to claim its service IAAS is ISO-27001 compliant:

* ISO-27001 compliance MUST be assessed. Those steps are out of Gaia-X.
  * ProviderA contact AUDITOR to asks ISO-27001 assessment for its IAAS service
  * AUDITOR runs assessment
* if AUDITOR considers service IAAS is ISO-27001 compliant
  * it creates and signs a ThirdPartyComplianceCertificateClaim IAAS-ISO-27001 indicating the IAAS service offering, AUDITOR and the ThirdPartyComplianceCertificationScheme
  * it creates and signs a ThirdPartyComplianceCertificateCredential referencing the ThirdPartyComplianceCertificateClaim IAAS-ISO-27001

